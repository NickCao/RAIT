package misc

import (
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"net"
	"reflect"
	"testing"
)

func TestLinkLocalAddress(t *testing.T) {
	var key, _ = wgtypes.ParseKey("JOlbUVIMjDVo3Ebo8iEAnhN2e+Nfhvlki5CydvMrCQs=")
	var addr = net.ParseIP("fe98:950c:a61e:8f3c:f641:7b7d:87d7:03d6")
	if !reflect.DeepEqual(LinkLocalAddress(key), addr) {
		t.Errorf("LinkLocalAddress test failed")
	}
}
