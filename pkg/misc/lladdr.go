package misc

import (
	"golang.org/x/crypto/blake2s"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"net"
)

var _, prefix, _ = net.ParseCIDR("fe80::/10")

func min(op1, op2 int) int {
	if op1 < op2 {
		return op1
	}
	return op2
}

func not(op1 []byte) (res []byte) {
	for i := 0; i < len(op1); i++ {
		res = append(res, ^op1[i])
	}
	return res
}

func and(op1, op2 []byte) (res []byte) {
	for i := 0; i < min(len(op1), len(op2)); i++ {
		res = append(res, op1[i]&op2[i])
	}
	return res
}

func or(op1, op2 []byte) (res []byte) {
	for i := 0; i < min(len(op1), len(op2)); i++ {
		res = append(res, op1[i]|op2[i])
	}
	return res
}

func LinkLocalAddress(key wgtypes.Key) net.IP {
	hash := blake2s.Sum256(key[:])
	return or(and(prefix.IP, prefix.Mask), and(hash[:], not(prefix.Mask)))
}
