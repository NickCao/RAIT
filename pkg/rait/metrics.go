package rait

import (
	"fmt"
	"github.com/Vivena/babelweb2/parser"
	"github.com/prometheus/client_golang/prometheus"
	"io"
	"log"
	"net"
	"strconv"
)

var neighborRttDesc = prometheus.NewDesc("babeld_neighbor_rtt", "rtt to neighbor", []string{"addr", "if"}, nil)
var interfaceCountDesc = prometheus.NewDesc("babeld_interface_count", "count of interface", []string{"up"}, nil)
var routeCountDesc = prometheus.NewDesc("babeld_route_count", "count of route", []string{"installed"}, nil)
var xrouteCountDesc = prometheus.NewDesc("babeld_xroute_count", "count of exported route", nil, nil)

func (b *Babeld) Describe(ch chan<- *prometheus.Desc) {
	ch <- neighborRttDesc
	ch <- interfaceCountDesc
	ch <- routeCountDesc
	ch <- xrouteCountDesc
}

func (b *Babeld) Collect(ch chan<- prometheus.Metric) {
	conn, err := net.Dial(b.SocketType, b.SocketAddr)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()

	_, err = fmt.Fprint(conn, "dump\nquit\n")
	if err != nil {
		log.Println(err)
		return
	}

	desc := parser.NewBabelDesc()
	scanner := parser.NewScanner(conn)

	var if_up, if_down, route_ins, route_bak, xroute_all int

	for {
		upd, err := desc.ParseAction(scanner)
		if err != nil && err.Error() != "EOL" {
			if err != io.EOF {
				log.Print(err)
			}
			break
		}
		supd := upd.ToSUpdate()
		switch supd.Action {
		case "add":
			switch supd.TableId {
			case "interface":
				up, ok := supd.EntryData["up"].(bool)
				if !ok {
					continue
				}
				if up {
					if_up++
				} else {
					if_down++
				}
			case "neighbour":
				rtts, ok := supd.EntryData["rtt"].(string)
				if !ok {
					continue
				}
				rtt, err := strconv.ParseFloat(rtts, 64)
				if err != nil {
					continue
				}
				addr, ok := supd.EntryData["address"].(string)
				if !ok {
					continue
				}
				itf, ok := supd.EntryData["if"].(string)
				if !ok {
					continue
				}
				ch <- prometheus.MustNewConstMetric(
					neighborRttDesc, prometheus.GaugeValue,
					rtt, addr, itf)
			case "xroute":
				xroute_all++
			case "route":
				installed, ok := supd.EntryData["installed"].(bool)
				if !ok {
					continue
				}
				if installed {
					route_ins++
				} else {
					route_bak++
				}
			default:
				continue
			}
		default:
			continue
		}
	}

	ch <- prometheus.MustNewConstMetric(
		interfaceCountDesc,
		prometheus.GaugeValue,
		float64(if_up),
		"true",
	)
	ch <- prometheus.MustNewConstMetric(
		interfaceCountDesc,
		prometheus.GaugeValue,
		float64(if_down),
		"false",
	)
	ch <- prometheus.MustNewConstMetric(
		routeCountDesc,
		prometheus.GaugeValue,
		float64(route_ins),
		"yes",
	)
	ch <- prometheus.MustNewConstMetric(
		routeCountDesc,
		prometheus.GaugeValue,
		float64(route_bak),
		"no",
	)
	ch <- prometheus.MustNewConstMetric(
		xrouteCountDesc,
		prometheus.GaugeValue,
		float64(xroute_all),
	)
}
