package rait

import (
	"gitlab.com/NickCao/RAIT/v4/pkg/entity"
	"gitlab.com/NickCao/RAIT/v4/pkg/misc"
)

func NewRAIT(path string) (*RAIT, error) {
	var r = &RAIT{
		Babeld: &Babeld{
			Enabled:    false,
			SocketType: "unix",
			SocketAddr: "/run/babeld.ctl",
			Param:      "type tunnel link-quality true split-horizon false rxcost 32 hello-interval 20 max-rtt-penalty 1024 rtt-max 1024",
			Footnote:   "interface host type wired",
		},
	}

	if err := misc.UnmarshalHCL(path, r); err != nil {
		return nil, err
	}
	return r, nil
}

func (r *RAIT) Sync(up bool) error {
	var peers []entity.Peer
	var err error
	if up {
		peers, err = r.Listing()
		if err != nil {
			return err
		}
	}

	var links []string
	for _, trans := range r.Transport {
		l, err := trans.PeersEnsure(r, peers)
		if err != nil {
			return err
		}
		links = append(links, l...)
	}

	err = r.Babeld.LinkSync(links)
	if err != nil {
		return err
	}
	return nil
}
